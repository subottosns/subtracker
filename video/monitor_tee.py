#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import pygame
import pygame.locals
import threading
import datetime
import socket
import socketserver
import queue
import time
import traceback
import tzlocal
import collections
import argparse

from imgio import read_jpeg_frame, write_jpeg_frame, decode_jpeg_to_rgb

parser = argparse.ArgumentParser()
parser.add_argument('width', type=int, help='width of the video stream')
parser.add_argument('height', type=int, help='height of the video stream')
parser.add_argument('-p', '--port', type=int, default=2204, help='TCP port')
parser.add_argument('-d', '--dir', default='./', help='TCP port')

args = parser.parse_args()

QUEUE_MAXSIZE = 500
HOST = "0.0.0.0"
PORT = args.port
CUT_EVERY = datetime.timedelta(minutes=3)

last_frame = None, None
fin = None
fouts_lock = threading.Lock()
fouts = []
finish = False
connections_lock = threading.Lock()
connections = []


class FPSEstimator:
    def __init__(self, span):
        self.queue = collections.deque()
        self.length = 0
        self.span = span
        self.lock = threading.Lock()

    def push_frame(self, timestamp):
        with self.lock:
            self.queue.append(timestamp)
            self.length += 1

            while self.queue[0] < timestamp - self.span:
                self.queue.popleft()
                self.length -= 1

            return float(self.length) / self.span

    def get_fps(self):
        with self.lock:
            return float(self.length) / self.span


fps_est = FPSEstimator(5.0)


# From https://docs.python.org/2/library/socketserver.html#asynchronous-mixins
class Connection(socketserver.BaseRequestHandler):
    def handle(self):
        print("New connection", file=sys.stderr)
        # We will never read from the socket, so we shut it down
        # immediately for reading
        self.request.shutdown(socket.SHUT_RD)
        self.queue = queue.Queue(QUEUE_MAXSIZE)
        fd = self.request.makefile('wb')
        try:
            with connections_lock:
                connections.append(self)
            while not finish:
                imdata, timestamp = self.queue.get(block=True)
                write_jpeg_frame(fd, imdata, timestamp)
                fd.flush()
        finally:
            with connections_lock:
                connections.remove(self)
            print("Connection closed", file=sys.stderr)
            fd.close()
            self.request.shutdown(socket.SHUT_WR)

    def enqueue_frame(self, imdata, timestamp):
        try:
            self.queue.put((imdata, timestamp), block=False)
            return True
        except queue.Full:
            print("Discarding frame because of a full queue", file=sys.stderr)
            return False


class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass


def copy():
    global fin, fouts, finish, last_frame
    try:
        while not finish:
            imdata, timestamp = read_jpeg_frame(fin)
            #print(timestamp)
            if timestamp is not None:
                fps_est.push_frame(timestamp)
            last_frame = (imdata, timestamp)
            with fouts_lock:
                for fout in fouts:
                    write_jpeg_frame(fout, imdata, timestamp)
            with connections_lock:
                for connection in connections:
                    connection.enqueue_frame(imdata, timestamp)
            time.sleep(0)
    except KeyboardInterrupt:
        print("Interrupt received in copy thread", file=sys.stderr)
    finally:
        finish = True


def main():
    global fin, fouts, finish, last_frame
    # Init PyGame
    size = (args.width, args.height)
    pygame.init()
    pygame.display.set_caption(sys.argv[0])
    surf = pygame.display.set_mode(size, pygame.DOUBLEBUF, 32)
    clock = pygame.time.Clock()
    font = pygame.font.SysFont('Inconsolata', 16)

    fin = sys.stdin.buffer
    fouts = []
    copy_thread = threading.Thread(target=copy)
    copy_thread.start()

    recording = False

    # Initialize ConnectionServer
    server = ThreadedTCPServer((HOST, PORT), Connection)
    server_thread = threading.Thread(target=server.serve_forever)
    server_thread.start()

    recording_began = None
    filename = None

    try:
        while not finish:
            # Process events
            toggle_recording = False
            for event in pygame.event.get():
                if event.type == pygame.locals.QUIT:
                    finish = True

                elif event.type == pygame.locals.KEYDOWN:
                    if event.key == pygame.locals.K_ESCAPE:
                        pygame.event.post(pygame.event.Event(pygame.locals.QUIT))

                    if event.str == 'r':
                        toggle_recording = True

            now = datetime.datetime.now()
            stop_recording = False
            start_recording = False
            if toggle_recording:
                if recording:
                    stop_recording = True
                else:
                    start_recording = True

            else:
                if recording and now >= recording_began + CUT_EVERY:
                    stop_recording = True
                    start_recording = True

            with fouts_lock:
                if stop_recording:
                    print("Closing record file", file=sys.stderr)
                    fouts[-1].close()
                    del fouts[-1]
                    recording = False
                    filename = None
                    recording_began = None
                if start_recording:
                    recording_began = datetime.datetime.now()
                    filename = recording_began.strftime(os.path.join(args.dir, "record-%Y%m%d-%H%M%S.gjpeg"))
                    print("Recording on %s" % (filename), file=sys.stderr)
                    fout = open(filename, 'w')
                    fouts.append(fout)
                    recording = True

            # Read image and show it
            if last_frame[0] is not None:
                try:
                    print("Frame received", file=sys.stderr)
                    imdata, timestamp = last_frame
                    image = decode_jpeg_to_rgb(imdata)
                    pygame.surfarray.blit_array(surf, image)
                    writing_pos = (0, 0)
                    font_surf = font.render("Recording on file %s" % (filename) if recording else "Not recording", True, pygame.Color(255, 0, 0) if recording else pygame.Color(255, 255, 255), pygame.Color(0, 0, 0))
                    surf.blit(font_surf, writing_pos)
                    writing_pos = (writing_pos[0], writing_pos[1] + font_surf.get_size()[1])
                    font_surf = font.render("Timestamp: %f" % (timestamp), True, pygame.Color(255, 255, 255), pygame.Color(0, 0, 0))
                    surf.blit(font_surf, writing_pos)
                    writing_pos = (writing_pos[0], writing_pos[1] + font_surf.get_size()[1])
                    font_surf = font.render("Time: %s" % (datetime.datetime.fromtimestamp(timestamp, tz=tzlocal.get_localzone())), True, pygame.Color(255, 255, 255), pygame.Color(0, 0, 0))
                    surf.blit(font_surf, writing_pos)
                    writing_pos = (writing_pos[0], writing_pos[1] + font_surf.get_size()[1])
                    font_surf = font.render("Real FPS: %f" % (fps_est.get_fps()), True, pygame.Color(255, 255, 255), pygame.Color(0, 0, 0))
                    surf.blit(font_surf, writing_pos)
                    writing_pos = (writing_pos[0], writing_pos[1] + font_surf.get_size()[1])
                    font_surf = font.render("Visualization FPS: %f" % (clock.get_fps()), True, pygame.Color(255, 255, 255), pygame.Color(0, 0, 0))
                    surf.blit(font_surf, writing_pos)
                    writing_pos = (writing_pos[0], writing_pos[1] + font_surf.get_size()[1])
                except:
                    print("Exception while showing the JPEG frame", file=sys.stderr)
                    traceback.print_exc(file=sys.stderr)

            pygame.display.flip()
            clock.tick(15)

    except KeyboardInterrupt:
        print("Interrupt received in main thread", file=sys.stderr)
    finally:
        finish = True
        pygame.quit()

    print("Shutting down server", file=sys.stderr)
    server.shutdown()
    server.server_close()

    print("Joining on the server thread", file=sys.stderr)
    server_thread.join()
    print("Joining on the copy thread", file=sys.stderr)
    copy_thread.join()
    print("Main thread quitting", file=sys.stderr)


if __name__ == '__main__':
    main()
