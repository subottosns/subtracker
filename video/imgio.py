#!/usr/bin/env python
# -*- coding: utf-8 -*-

import struct
import sys

from turbojpeg import TurboJPEG, TJPF_RGBX, TJPF_RGB

# import PIL.Image
#
# def pil_open_from_data(data):
#     return PIL.Image.open(StringIO.StringIO(data))
#
# def pil_write_to_data(image):
#     fout = io.StringIO()
#     image.save(fout, "jpeg")
#     return fout.getvalue()


def read_jpeg_frame(fin):
    timestamp_tag = fin.read(8)
    #print("aasff", file=sys.stderr)
    if len(timestamp_tag) == 0:
        return None, None
    length_tag = fin.read(4)
    timestamp, = struct.unpack("d", timestamp_tag)
    length, = struct.unpack("!I", length_tag)
    imdata = fin.read(length)

    return imdata, timestamp


def write_jpeg_frame(fout, imdata, timestamp):
    timestamp_tag = struct.pack("d", timestamp)
    length = len(imdata)
    length_tag = struct.pack("!I", length)
    fout.write(timestamp_tag)
    fout.write(length_tag)
    fout.write(imdata)


# See https://stackoverflow.com/questions/1557071/the-size-of-a-jpegjfif-image for information
SOI_TAG = b'\xff\xd8'
EOI_TAG = b'\xff\xd9'
SOS_TAG = b'\xff\xda'
TAG_WITHOUT_LENGTH = [b'\xff\xd8',
                      b'\xff\x01',
                      b'\xff\xd0',
                      b'\xff\xd1',
                      b'\xff\xd2',
                      b'\xff\xd3',
                      b'\xff\xd4',
                      b'\xff\xd5',
                      b'\xff\xd6',
                      b'\xff\xd7',
                      b'\xff\xd9',
                  ]


def read_unbounded_jpeg_frame(fin):
    data = []

    # Read the SOI tag (or check that the stream was finished)
    tag = fin.read(2)
    data.append(tag)
    if tag == '':
        return
    assert tag == SOI_TAG

    read_tag = None

    while True:
        # Read the tag
        if read_tag is None:
            tag = fin.read(2)
            data.append(tag)
        else:
            tag = read_tag
        #print(repr(tag), file=sys.stderr)
        assert len(tag) == 2
        assert tag[0:1] == b'\xff'

        # If we have EOI, we're done!
        if tag == EOI_TAG:
            break

        # Some tag have no payload, skip them
        if tag in TAG_WITHOUT_LENGTH:
            continue

        # Read payload length
        length_bytes = fin.read(2)
        assert len(length_bytes) == 2
        data.append(length_bytes)
        length, = struct.unpack("!H", length_bytes)

        # Read payload
        payload = fin.read(length - 2)
        assert len(payload) == length - 2
        data.append(payload)

        # SOS packets have additional data after the payload; it's ok
        # to search for a new marker, because markers cannot occur in
        # such data
        read_tag = None
        if tag == SOS_TAG:
            prev_ff = False
            while True:
                byte = fin.read(1)
                data.append(byte)
                #print >> sys.stderr, repr(byte),
                if prev_ff and byte != b'\x00':
                    read_tag = b'\xff' + byte
                    break
                prev_ff = (byte == b'\xff')


    return b"".join(data)


jpeg = TurboJPEG()


def decode_jpeg_to_rgb(frame):
    return jpeg.decode(frame, pixel_format=TJPF_RGB).transpose(1,0,2)
