#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import time

from imgio import write_jpeg_frame, read_unbounded_jpeg_frame


def main():
    num = 0
    try:
        while True:
            content = read_unbounded_jpeg_frame(sys.stdin.buffer)
            if content is None:
                break
            write_jpeg_frame(sys.stdout.buffer, content, time.time())
            # with open("tmp/out{}.jpeg".format(num), "w") as f:
            #     write_jpeg_frame(f, content, time.time())
            print(num, file=sys.stderr)
            num += 1
    except KeyboardInterrupt:
        pass


if __name__ == '__main__':
    main()
