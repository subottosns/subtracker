# Subtracker #
Credits to Giovanni Mascellani, Giovanni Paolini e altri.  
Repository originale: https://github.com/subotto/subtracker

Software per il tracciamento della pallina in un biliardino.

# Istruzioni per l'uso #

## Compilare ##

### OpenCV ###
* Fate l'update dei submodules se non è già stato fatto
* Create la cartella `my_opencv/opencv_install`
* Date il comando `cd my_opencv/opencv && git apply ../ffmpeg.patch`
* Create ed entrate nella cartella `my_opencv/opencv/build`
* Date il comando contenuto nel file `my_opencv/opencv_cmake_line.txt`
* Lanciate `make && make install`
* Ora avete un'installazione custom di OpenCV in `my_opencv/opencv_install`!

### Qt Subtracker ###
* Installare i seguenti pacchetti: `qt5-default build-essential cmake qt5-qmake qtbase5-dev-tools libturbojpeg0-dev pkg-config libboost-all-dev`
* Entrare nella cartella `qt` e dare il comando `source var.sh` (esporta le variabili per lavorare con il nostro OpenCV)
* Eseguire `qmake`
* Dare `make`: l'eseguibile sarà in `qt/bin/Subtracker`

### Script video helper ###
(TODO: trovare delle librerie sensate)
* Installare i pacchetti `python-dev libturbojpeg0-dev build-essential libcairo2-dev`
* Entrare nella cartella `video` e dare `make`: questo builderà `v4l2_source`
* Creare un virtualenv Python3 e installare `video/requirements.txt`


## Usare ##
* Collegare la webcam giusta via USB.
* Utilizzare il programma `video/v4l2_source` per interfacciarsi con la webcam e scrivere frame in formato JPEG su standard output.
Opzioni utili sono: `-d DEVICE` (per indicare la webcam), `-a` (si riavvia automaticamente ad ogni crash, per esempio se la webcam viene scollegata e ricollegata), `-f` (forza la risoluzione 640x480 e formato MPEG).
* Mandare i frame in pipe a `video/monitor_tee.py`, il quale ascolta (e manda i frame) su una porta TCP (default 2204) e opzionalmente fa il dump sull'hard disk per l'archiviazione (serve tanta memoria, ordine dei 500 GB).
L'archiviazione dei frame su disco è attivata/disattivata premendo il tasto `r` (crea un file da 1 GB ogni 3 minuti circa).
Riassumendo, un comando ragionevole è `./v4l2_source -d /dev/video0 -a -f | ./monitor_tee.py 640 480`.
Opzioni utili di `monitor_tee.py` sono: `-p PORT` (per indicare la porta), `-d DIRECTORY` (cartella per l'archiviazione).
* In alternativa, se si sta testando con video vecchi, usare il comando `ffmpeg -i /path/to/recording -vcodec mjpeg -f image2pipe - | ./mjpeg_source.py | ./monitor_tee.py 640 480`. Forse con i video generati dal `monitor_tee` del passato non serve ffmpeg.
* Riempire `settings.yaml` con i dati relativi alle immagini di riferimento.
* Eseguire `Subtracker` (che si connette a localhost sulla porta 2204).
* Redirigere l'output di `Subtracker` a `web/insert.py` per inserire i dati nel database (controllare di avere più di 1 GB libero).
Comando completo: `./Subtracker | (cd ~/subtracker/web/; ./insert.py)`
* TODO: servizio che rende disponibili i dati ai client HTTP.
