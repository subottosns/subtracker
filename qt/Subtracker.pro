
QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Subtracker
TEMPLATE = app


SOURCES += src/main.cpp\
        src/mainwindow.cpp \
    src/videowidget.cpp \
    src/framereader.cpp \
    src/logging.cpp \
    src/worker.cpp \
    src/context.cpp \
    src/frameanalysis.cpp \
    src/categorybutton.cpp \
    src/ballpanel.cpp \
    src/treesubframe.cpp \
    src/foosmenpanel.cpp \
    src/beginningpanel.cpp \
    src/memory.c \
    src/debugpanel.cpp \
    src/cv.cpp \
    src/coordinates.cpp \
    src/frameanalysis_tracking.cpp \
    src/spotstracker.cpp

HEADERS  += src/mainwindow.h \
    src/videowidget.h \
    src/framereader.h \
    src/logging.h \
    src/worker.h \
    src/context.h \
    src/frameanalysis.h \
    src/atomiccounter.h \
    src/framesettings.h \
    src/categorybutton.h \
    src/ballpanel.h \
    src/treesubframe.h \
    src/foosmenpanel.h \
    src/beginningpanel.h \
    src/memory.h \
    src/framewaiter.h \
    src/coordinates.h \
    src/debugpanel.h \
    src/cv.h \
    src/spotstracker.h

FORMS    += src/forms/mainwindow.ui \
    src/forms/ballpanel.ui \
    src/forms/foosmenpanel.ui \
    src/forms/beginningpanel.ui \
    src/forms/debugpanel.ui

DESTDIR=bin
OBJECTS_DIR=build/obj
MOC_DIR=build/moc
UI_DIR=build/ui

QMAKE_CXXFLAGS += -std=c++17 -march=native -g -DBOOST_ALL_DYN_LINK
#QMAKE_LFLAGS += -lturbojpeg -lboost_log_setup -lboost_log -lboost_system -lboost_thread
INCLUDEPATH += src/ #$$system(pkg-config --cflags opencv)
LIBS += -lturbojpeg -lboost_log_setup -lboost_log -lboost_system -lboost_thread #$$system(pkg-config --libs opencv)

# We do not use qmake own system for interfacing with pkg-config, because I cannot make it honour PKG_CONFIG_PATH which is essential for using our own version of OpenCV
CONFIG+=link_pkgconfig
PKGCONFIG+=opencv
