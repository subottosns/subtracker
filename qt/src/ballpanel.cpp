#include "ballpanel.h"
#include "ui_ballpanel.h"

BallPanel::BallPanel(MainWindow *main, QWidget *parent) :
    TreeSubFrame(main, parent),
    ui(new Ui::BallPanel)
{
    ui->setupUi(this);
    this->set_main_on_children();
}

BallPanel::~BallPanel()
{
    delete ui;
}

void BallPanel::receive_frame(QSharedPointer<FrameAnalysis> frame)
{
    this->ui->ballLL->set_current_frame(frame->objects_ll[2]);
    this->ui->ballMasked->set_current_frame(frame->ball_masked);
    this->ui->ballTBlur->set_current_frame(frame->ball_tblur);
}

static void set_slider(QSlider *slider, int min, int max, int value) {
    slider->setMinimum(min);
    slider->setMaximum(max);
    slider->setValue(value);
}

static void set_color_slider(QSlider *slider, float value) {
    set_slider(slider, 0, 255, static_cast<int>(roundf(value * 255.0f)));
}

void BallPanel::receive_settings(const FrameSettings &settings)
{
    set_color_slider(this->ui->ballR, settings.objects_colors[2][2]);
    set_color_slider(this->ui->ballG, settings.objects_colors[2][1]);
    set_color_slider(this->ui->ballB, settings.objects_colors[2][0]);
    set_color_slider(this->ui->ballStdDev, settings.objects_color_stddev[2]);
    set_slider(this->ui->ballTableThresh, 0, 20, settings.table_nll_threshold);
    set_slider(this->ui->ballOllThresh, 0, 20, settings.oll_threshold);
    set_slider(this->ui->ballBlurThresh, 0, 255, settings.ball_ll_threshold);
    set_slider(this->ui->ballDilateSize, 1, 20, settings.dilation_size);
}

void BallPanel::on_ballR_valueChanged(int value)
{
    this->main->get_settings().objects_colors[2][2] = static_cast<float>(value) / 255.0f;
    this->main->settings_modified();
}

void BallPanel::on_ballG_valueChanged(int value)
{
    this->main->get_settings().objects_colors[2][1] = static_cast<float>(value) / 255.0f;
    this->main->settings_modified();
}

void BallPanel::on_ballB_valueChanged(int value)
{
    this->main->get_settings().objects_colors[2][0] = static_cast<float>(value) / 255.0f;
    this->main->settings_modified();
}

void BallPanel::on_ballStdDev_valueChanged(int value)
{
    this->main->get_settings().objects_color_stddev[2] = static_cast<float>(value) / 255.0f;
    this->main->settings_modified();
}

void BallPanel::on_ballTableThresh_valueChanged(int value)
{
    this->main->get_settings().table_nll_threshold = value;
    this->main->settings_modified();
}

void BallPanel::on_ballBlurThresh_valueChanged(int value)
{
    this->main->get_settings().ball_ll_threshold = value;
    this->main->settings_modified();
}

void BallPanel::on_ballOllThresh_valueChanged(int value)
{
    this->main->get_settings().oll_threshold = value;
    this->main->settings_modified();
}

void BallPanel::on_ballDilateSize_valueChanged(int value)
{
    this->main->get_settings().dilation_size = value;
    this->main->settings_modified();
}
